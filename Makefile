install:
	npm install -g @nestjs/cli
	npm install

run-database:
	docker-compose up -d mongo

dev:
	npm run start:dev

prod:
	npm run build
	npm run start:prod

clean:
	bash clean.sh

