import { Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { Request as RequestInterface } from 'express';
import { AppService } from './app.service';
import { User } from './modules/user/user.service';
import { AuthService } from './utils/auth/auth.service';
import { LocalGuard } from './utils/auth/local-auth.guard';

interface LoginRequest extends RequestInterface {
  user: User;
}

interface LoginResponse {
  access_token: string;
}

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly authService: AuthService,
  ) {}

  @Get()
  getInitialInfo(): { status: string } {
    return this.appService.InitialInfo();
  }

  @UseGuards(LocalGuard)
  @Post('login')
  async login(@Request() req: LoginRequest): Promise<LoginResponse> {
    return this.authService.login(req.user);
  }
}
