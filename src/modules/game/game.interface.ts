export interface GameInterface {
  readonly pictures: string[];
  readonly token: string;
}
