import { Injectable } from '@nestjs/common';
import { GameRequestDTO } from './game.dto';
import { GameInterface } from './game.interface';

// Mock data
// TODO search different images
const images = [
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
  'https://www.gannett-cdn.com/media/USATODAY/GenericImages/2012/11/26/gan-grumpy-cat-112612-1-16_9.jpg?width=1807&height=1020&fit=crop&format=pjpg&auto=webp',
];

@Injectable()
export class GameService {
  GetGame = async (dto: GameRequestDTO): Promise<GameInterface> => {
    const { size } = dto;
    const imageNumber = size / 2;

    const pictures = [];
    for (let index = 0; index < imageNumber; index++) {
      pictures.push(images[index]);
    }

    return {
      pictures,
      token: 'some token',
    };
  };
}
