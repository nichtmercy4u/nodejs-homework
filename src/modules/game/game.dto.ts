import { IsInt, Min, Max, IsDivisibleBy } from 'class-validator';

export class GameRequestDTO {
  @IsInt()
  @Min(5)
  @Max(20)
  @IsDivisibleBy(2)
  size: number;
}
