import {
  BadRequestException,
  Controller,
  Get,
  Param,
  UseGuards,
} from '@nestjs/common';
import { GameInterface } from './game.interface';
import { GameService } from './game.service';
import { validate } from 'class-validator';
import { GameRequestDTO } from './game.dto';
import { JwtAuthGuard } from 'src/utils/auth/jwt-auth.guard';

@Controller('game')
export class GameController {
  constructor(private readonly service: GameService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/:size')
  async getGame(@Param('size') size: string): Promise<GameInterface> {
    const dto = new GameRequestDTO();
    dto.size = parseInt(size, 10);
    const validationError = await validate(dto);

    if (validationError.length) throw new BadRequestException(validationError);

    return this.service.GetGame(dto);
  }
}
