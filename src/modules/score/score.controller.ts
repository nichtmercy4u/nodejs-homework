import { Body, Controller, Post, Get } from '@nestjs/common';
import { WriteScoreDTO } from './score.dto';
import {
  AllScoreResponseInterface,
  ScoreResponseInterface,
} from './score.interface';
import { ScoreService } from './score.service';

@Controller('score')
export class ScoreController {
  constructor(private readonly service: ScoreService) {}

  @Post()
  async createScore(
    @Body() body: WriteScoreDTO,
  ): Promise<ScoreResponseInterface> {
    return this.service.submitScore(body);
  }

  @Get()
  async getAllScore(): Promise<AllScoreResponseInterface[]> {
    return this.service.getAllScore();
  }
}
