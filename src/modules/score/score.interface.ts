import { Document } from 'mongoose';

export interface ScoreInterface extends Document {
  readonly steps: number;
  readonly seconds: number;
  readonly name: string;
}

export interface ScoreResponseInterface {
  readonly position: number;
}

export interface AllScoreResponseInterface {
  readonly _id: string;
  readonly name: string;
  readonly steps: number;
  readonly seconds: number;
}
