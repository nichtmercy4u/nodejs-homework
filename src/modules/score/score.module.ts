import { Module } from '@nestjs/common';
import { ScoreService } from './score.service';
import { ScoreController } from './score.controller';
import ScoreModel from './score.model';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ScoreModel, ConfigModule],
  providers: [ScoreService],
  controllers: [ScoreController],
})
export class ScoreModule {}
