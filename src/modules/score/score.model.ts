import { MongooseModule } from '@nestjs/mongoose';
import { Schema } from 'mongoose';

const ScoreSchema = new Schema(
  {
    steps: { type: Number, required: true },
    seconds: { type: Number, required: true },
    name: { type: String, required: true },
  },
  {
    timestamps: true,
  },
);

const ScoreModel = MongooseModule.forFeature([
  { name: 'score', schema: ScoreSchema },
]);
export default ScoreModel;
