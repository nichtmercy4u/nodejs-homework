import { IsString, IsNumber } from 'class-validator';

export class WriteScoreDTO {
  @IsNumber() readonly steps: number;
  @IsNumber() readonly seconds: number;
  @IsString() readonly name: string;
}
