import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  AllScoreResponseInterface,
  ScoreInterface,
  ScoreResponseInterface,
} from './score.interface';
import { Model } from 'mongoose';
import { WriteScoreDTO } from './score.dto';

@Injectable()
export class ScoreService {
  constructor(
    @InjectModel('score') private readonly model: Model<ScoreInterface>,
  ) {}

  submitScore = async (dto: WriteScoreDTO): Promise<ScoreResponseInterface> => {
    const scoreDocument = new this.model(dto);
    const currentScore = await scoreDocument.save();

    const sortedScores: AllScoreResponseInterface[] = await this.model
      .find()
      .sort({ steps: 1, seconds: 1 });
    const position = sortedScores
      .map(item => item._id.toString())
      .indexOf(currentScore._id.toString());

    // Add + 1 to get the position not the index
    return { position: position + 1 };
  };

  getAllScore = async (): Promise<AllScoreResponseInterface[]> => {
    const sortedScores = await this.model.find().sort({ steps: 1, seconds: 1 });
    return sortedScores;
  };
}
