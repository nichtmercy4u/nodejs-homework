import Boom, { boomify } from 'boom';
import { Catch, ArgumentsHost } from '@nestjs/common';
import { HttpException } from '@nestjs/common';

@Catch(HttpException)
export class ErrorFilter {
  catch(exception: HttpException, host: ArgumentsHost): string {
    const ctx = host.switchToHttp();
    const status = exception.getStatus();
    const response = ctx.getResponse();
    const boomifiedError: Boom = boomify(exception, {
      statusCode: status || 500,
      override: true,
    });

    if (exception.message)
      boomifiedError.output.payload.message = exception.message;

    return response.status(status).json(exception);
  }
}
