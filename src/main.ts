import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import { AppModule } from './app.module';
import { ErrorFilter } from './utils/filters/error.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  /**
   * Query and body validator
   * https://github.com/typestack/class-validator
   */
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));

  /**
   * Enable CORS
   * https://docs.nestjs.com/techniques/security
   */
  app.enableCors();

  /**
   * Boom error handler
   * https://github.com/hapijs/boom
   */
  app.useGlobalFilters(new ErrorFilter());

  /**
   * Security
   * https://docs.nestjs.com/techniques/security
   */
  app.use(helmet());

  /**
   * Logger
   * https://www.npmjs.com/package/morgan
   */
  const loggerLevel =
    process.env.NODE_ENV === 'development' ? 'dev' : 'combined';
  app.use(morgan(loggerLevel));

  await app.listen(3000);
}
bootstrap();
