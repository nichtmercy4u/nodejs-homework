# Nodejs homework

## Description
Card-matching game API server

I used [NestJS](https://nestjs.com) for server framework
and [MongoDB](https://www.mongodb.com) for databse

## Requirements
You need [NodeJS](https://nodejs.org/en/) and 
[DOCKER](https://docs.docker.com/get-docker/) in your machine

## Install
```bash
$ make install
$ make run-database
```

## Running the app in development
```bash
$ make dev
```

## Running the app in production
```bash
$ make prod
```

App will be available at `http://localhost:3000`

## Clean the environment
```bash
$  make clean
```
